;;; Guix Past --- Packages from the past for GNU Guix.
;;; Copyright © 2021, 2022, 2024 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This file is part of Guix Past.
;;;
;;; Guix Past is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guix Past is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guix Past.  If not, see <http://www.gnu.org/licenses/>.

(define-module (past packages bioinformatics)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix build-system r)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages statistics)
  #:use-module (past packages python27))

(define S specification->package)

(define-public htslib-1.0
  (package/inherit htslib
    (name "htslib")
    (version "1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/samtools/htslib/")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hq4gghs2n7v70zpsbnxin4sikaafwmicq19x630ys243wr6ib3k"))))
    (arguments
     (list
      #:make-flags
      #~(list (string-append "prefix=" #$output))
      #:phases
      '(modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'patch-tests
           (lambda _
             (substitute* "test/test.pl"
               (("/bin/bash") (which "bash"))))))))))

;; Needed for pacbio-htslib
(define-public htslib-1.1
  (package/inherit htslib-1.0
    (name "htslib")
    (version "1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/samtools/htslib/")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "001if152qcpl2p2qiz053zkckl8yqm8031z5j6n1n01dchzy9bql"))))))

(define-public htslib-1.2.1
  (package/inherit htslib-1.0
    (name "htslib")
    (version "1.2.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/samtools/htslib/"
                                  "releases/download/"
                                  version "/htslib-"
                                  version ".tar.bz2"))
              (sha256
               (base32
                "1c32ssscbnjwfw3dra140fq7riarp2x990qxybh34nr1p5r17nxx"))))))

(define-public htslib-1.3
  (package/inherit htslib
    (version "1.3.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/samtools/htslib/releases/download/"
                    version "/htslib-" version ".tar.bz2"))
              (sha256
               (base32
                "1rja282fwdc25ql6izkhdyh8ppw8x2fs0w0js78zgkmqjlikmma9"))))))

(define-public htslib-1.4
  (package/inherit htslib
    (version "1.4.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/samtools/htslib/releases/download/"
                    version "/htslib-" version ".tar.bz2"))
              (sha256
               (base32
                "1crkk79kgxcmrkmh5f58c4k93w4rz6lp97sfsq3s6556zxcxvll5"))))))

(define-public htslib-1.5
  (package/inherit htslib
    (version "1.5")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/samtools/htslib/releases/download/"
                    version "/htslib-" version ".tar.bz2"))
              (sha256
               (base32
                "0bcjmnbwp2bib1z1bkrp95w9v2syzdwdfqww10mkb1hxlmg52ax0"))))))

(define-public bamm
  (package
    (name "bamm")
    (version "1.7.3")
    (source (origin
              (method git-fetch)
              ;; BamM is not available on pypi.
              (uri (git-reference
                    (url "https://github.com/Ecogenomics/BamM")
                    (commit version)
                    (recursive? #t)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1p83ahi984ipslxlg4yqy1gdnya9rkn1v71z8djgxkm9d2chw4c5"))
              (modules '((guix build utils)))
              (snippet
               `(begin
                  ;; Delete bundled htslib.
                  (delete-file-recursively "c/htslib-1.3.1")))))
    (build-system python-build-system)
    (arguments
     `(#:python ,python-2               ; BamM is Python 2 only.
       ;; Do not use bundled libhts.  Do use the bundled libcfu because it has
       ;; been modified from its original form.
       #:configure-flags
       ,#~(let ((htslib #$(this-package-input "htslib")))
            (list "--with-libhts-lib" (string-append htslib "/lib")
                  "--with-libhts-inc" (string-append htslib "/include/htslib")))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'autogen
           (lambda _
             (with-directory-excursion "c"
               (let ((sh (which "sh")))
                 (for-each make-file-writable (find-files "." ".*"))
                 ;; Use autogen so that 'configure' works.
                 (substitute* "autogen.sh" (("/bin/sh") sh))
                 (setenv "CONFIG_SHELL" sh)
                 (invoke "./autogen.sh")))))
         (delete 'build)                ;the build loops otherwise
         (replace 'check
           (lambda _
             ;; There are 2 errors printed, but they are safe to ignore:
             ;; 1) [E::hts_open_format] fail to open file ...
             ;; 2) samtools view: failed to open ...
             (invoke "nosetests")))
         (add-after 'install 'wrap-executable
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (path (getenv "PATH"))
                    (pythonpath (getenv "GUIX_PYTHONPATH")))
               (wrap-program (string-append out "/bin/bamm")
                 `("PATH" ":" prefix (,path))
                 `("GUIX_PYTHONPATH" ":" prefix (,pythonpath)))))))))
    (native-inputs
     (list (S "autoconf")
           (S "automake")
           (S "libtool")
           (S "zlib")
           (python2-package (S "python-nose"))
           python2-pysam))
    (inputs
     (list htslib-1.3        ; At least one test fails on htslib-1.4+.
           (S "samtools")
           (S "bwa")
           (S "grep")
           (S "sed")
           (S "coreutils")))
    (propagated-inputs
     (list (python2-package (S "python-numpy"))))
    (home-page "https://ecogenomics.github.io/BamM/")
    (synopsis "Metagenomics-focused BAM file manipulator")
    (description
     "BamM is a C library, wrapped in Python, to efficiently generate and
parse BAM files, specifically for the analysis of metagenomic data.  For
instance, it implements several methods to assess contig-wise read coverage.")
    (license license:lgpl3+)))

(define-public bcftools-1.5
  (package/inherit bcftools
    (version "1.5")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/samtools/bcftools/releases/download/"
                    version "/bcftools-" version ".tar.bz2"))
              (sha256
               (base32
                "0093hkkvxmbwfaa7905s6185jymynvg42kq6sxv7fili11l5mxwz"))
              (modules '((guix build utils)))
              (snippet
               ;; Delete bundled htslib.
               '(delete-file-recursively "htslib-1.5"))))
    (arguments
     (list
      #:test-target "test"
      #:configure-flags
      #~(list "--enable-libgsl"
              (string-append "--with-htslib="
                             #$(this-package-input "htslib")))
      #:phases
      '(modify-phases %standard-phases
         (add-before 'check 'patch-tests
           (lambda _
             (substitute* "test/test.pl"
               (("/bin/bash") (which "bash"))))))))
    (native-inputs (list (S "perl")))
    (inputs (list (S "gsl") htslib-1.5 (S "zlib")))))

(define-public couger
  (package
    (name "couger")
    (version "1.8.2")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "http://couger.oit.duke.edu/static/assets/COUGER"
                    version ".zip"))
              (sha256
               (base32
                "04p2b14nmhzxw5h72mpzdhalv21bx4w9b87z0wpw0xzxpysyncmq"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f                       ; there are none
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (delete 'build)
          (replace 'install
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((bin (string-append #$output "/bin")))
                (copy-recursively "src" (string-append #$output "/src"))
                (mkdir bin)
                ;; Add "src" directory to module lookup path.
                (substitute* "couger"
                  (("from argparse")
                   (string-append "import sys\nsys.path.append(\""
                                  #$output "\")\nfrom argparse")))
                (install-file "couger" bin))))
          (add-after 'install 'wrap-program
            (lambda _
              ;; Make sure 'couger' runs with the correct GUIX_PYTHONPATH.
              (let ((path (getenv "GUIX_PYTHONPATH")))
                (wrap-program (string-append #$output "/bin/couger")
                  `("GUIX_PYTHONPATH" ":" prefix (,path)))))))))
    (inputs
     (list (S "python2")
           (python2-package (S "python-pillow"))
           (python2-package (S "python-numpy"))
           (python2-package (S "python-scipy"))
           (python2-package (S "python-matplotlib"))))
    (propagated-inputs
     (map S (list "r-minimal" "libsvm" "randomjungle")))
    (native-inputs
     (map S (list "unzip")))
    (home-page "http://couger.oit.duke.edu")
    (synopsis "Identify co-factors in sets of genomic regions")
    (description
     "COUGER can be applied to any two sets of genomic regions bound
by paralogous TFs (e.g., regions derived from ChIP-seq experiments) to
identify putative co-factors that provide specificity to each TF.  The
framework determines the genomic targets uniquely-bound by each TF,
and identifies a small set of co-factors that best explain the in vivo
binding differences between the two TFs.

COUGER uses classification algorithms (support vector machines and
random forests) with features that reflect the DNA binding
specificities of putative co-factors.  The features are generated
either from high-throughput TF-DNA binding data (from protein binding
microarray experiments), or from large collections of DNA motifs.")
    (license license:gpl3+)))

(define-public samtools-0.1.8
  (package/inherit samtools-0.1
    (version "0.1.8")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "mirror://sourceforge/samtools/samtools/"
                       version "/samtools-" version ".tar.bz2"))
       (sha256
        (base32
         "16js559vg13zz7rxsj4kz2l96gkly8kdk8wgj9jhrqwgdh7jq9iv"))))))

(define-public samtools-1.1
  (package/inherit samtools-0.1
    (version "1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://sourceforge/samtools/samtools/"
                           version "/samtools-" version ".tar.bz2"))
       (sha256
        (base32
         "1y5p2hs4gif891b4ik20275a8xf3qrr1zh9wpysp4g8m0g1jckf2"))))))

(define-public samtools-1.3
  (package/inherit samtools
    (version "1.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://sourceforge/samtools/samtools/"
                           version "/samtools-" version ".tar.bz2"))
       (sha256
        (base32
         "0znnnxc467jbf1as2dpskrjhfh8mbll760j6w6rdkwlwbqsp8gbc"))))
    (inputs
     (modify-inputs (package-inputs samtools)
       (replace "htslib" htslib-1.3)))))

(define-public samtools-1.4
  (package/inherit samtools
    (version "1.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://sourceforge/samtools/samtools/"
                           version "/samtools-" version ".tar.bz2"))
       (sha256
        (base32
         "0vzxjm5vkgvzynl7cssm1l560rqs2amdaib1x8sp2ch9b7bxx9xx"))))
    (inputs
     (modify-inputs (package-inputs samtools)
       (replace "htslib" htslib-1.4)))))

(define-public qtltools-old
  (package
    (name "qtltools")
    (version "1.0")
    (source (origin
              (method url-fetch/tarbomb)
              (uri (string-append "https://qtltools.github.io/qtltools/"
                                  "binaries/QTLtools_" version
                                  "_source.tar.gz"))
              (sha256
               (base32
                "1drckp02jgpl8lswa09w10xa6fyd7r8nlg08yhg6c5hls0zbm277"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f                       ;no tests included
      #:make-flags
      #~(list (string-append "BOOST_INC="
                             #$(this-package-input "boost") "/include")
              (string-append "BOOST_LIB="
                             #$(this-package-input "boost") "/lib")
              (string-append "HTSLD_INC="
                             #$(this-package-input "htslib") "/include")
              (string-append "HTSLD_LIB="
                             #$(this-package-input "htslib") "/lib")
              (string-append "RMATH_INC="
                             #$(this-package-input "rmath-standalone")
                             "/include")
              (string-append "RMATH_LIB="
                             #$(this-package-input "rmath-standalone")
                             "/lib"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'fix-linkage
            (lambda _
              (substitute* "Makefile"
                (("-Wl,-Bstatic ") "")
                (("-Wl,-Bdynamic ") "")
                (("libboost_iostreams.a")
                 "libboost_iostreams.so")
                (("libboost_program_options.a")
                 "libboost_program_options.so")
                (("-lblas") "-lopenblas"))
              (substitute* "Makefile"
                (("LIB_FLAGS=-lz")
                 "LIB_FLAGS=-lz -lcrypto -lssl -lcurl")
                (("LIB_FILES=\\$\\(RMATH_LIB\\)/libRmath.a \
\\$\\(HTSLD_LIB\\)/libhts.a \
\\$\\(BOOST_LIB\\)/libboost_iostreams.a \
\\$\\(BOOST_LIB\\)/libboost_program_options.a")
                 "LIB_FILES=$(RMATH_LIB)/libRmath.so \
$(HTSLD_LIB)/libhts.so \
$(BOOST_LIB)/libboost_iostreams.so \
$(BOOST_LIB)/libboost_program_options.so"))))
          (delete 'configure)
          (replace 'install
            (lambda _
              (install-file "bin/QTLtools"
                            (string-append #$output "/bin")))))))
    (inputs
     (list (S "boost")
           (S "curl")
           (S "gsl")
           htslib-1.3
           (S "openblas")
           (S "openssl")
           (S "rmath-standalone")
           (S "zlib")))
    (home-page "https://qtltools.github.io/qtltools/")
    (synopsis "Tool set for molecular QTL discovery and analysis")
    (description "QTLtools is a tool set for molecular QTL discovery
and analysis.  It allows going from the raw genetic sequence data to
collection of molecular @dfn{Quantitative Trait Loci} (QTLs) in few
easy-to-perform steps.")
    (license license:gpl3+)))

(define-public r-sccustomize
  (let ((commit "8414d1f5fb32277855b0619191a568932b7baeb0")
        (revision "1"))
    (package
      (name "r-sccustomize")
      (version (git-version "0.7.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/samuel-marsh/scCustomize")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1wcgfq7lx83a2kf8pjbw524gdvxf351n08cwd5wzmmy57kf4knbj"))))
      (properties `((upstream-name . "scCustomize")))
      (build-system r-build-system)
      (propagated-inputs
       (map S (list "r-circlize"
                    "r-colorway"
                    "r-cowplot"
                    "r-data-table"
                    "r-dittoseq"
                    "r-dplyr"
                    "r-forcats"
                    "r-ggbeeswarm"
                    "r-ggplot2"
                    "r-ggprism"
                    "r-ggpubr"
                    "r-ggrastr"
                    "r-ggrepel"
                    "r-glue"
                    "r-janitor"
                    "r-magrittr"
                    "r-matrix"
                    "r-paletteer"
                    "r-patchwork"
                    "r-pbapply"
                    "r-purrr"
                    "r-remotes"
                    "r-scales"
                    "r-scattermore"
                    "r-seurat@4"
                    "r-seuratobject@4"
                    "r-stringi"
                    "r-stringr"
                    "r-tibble"
                    "r-tidyr"
                    "r-tidyselect"
                    "r-viridis")))
      (native-inputs (map S (list "r-knitr")))
      (home-page "https://github.com/samuel-marsh/scCustomize")
      (synopsis "Custom visualization and analyses of single-cell sequencing")
      (description
       "This is a collection of functions created and/or curated to aid in the
visualization and analysis of single-cell data using R.")
      (license license:gpl3+))))
