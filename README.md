Guix-Past: Bringing software from the past to the present
=================================================================

This repository has moved to
https://codeberg.org/guix-science/guix-past, see you there!
